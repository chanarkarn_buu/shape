/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.shape;

/**
 *
 * @author A_R_T
 */
public class Sqaure extends Shape {

    private double n;

    public Sqaure(double n) {
        super("Sqaure");
        this.n = n;
    }

    public double getN() {
        return n;
    }

    public void setN(double n) {
        this.n = n;
    }

    @Override
    public double calArea() {
        return n * n;
    }

    @Override
    public String toString() {
        return "Sqaure{" + "n=" + n + '}';
    }

}
