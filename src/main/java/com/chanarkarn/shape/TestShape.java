/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.shape;

/**
 *
 * @author A_R_T
 */
public class TestShape {

    public static void main(String[] args) {
        Circle c1 = new Circle(1.5);
        Circle c2 = new Circle(4.5);
        Circle c3 = new Circle(5.5);
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        //Sqaure
        Sqaure s1 = new Sqaure(2.0);
        Sqaure s2 = new Sqaure(4.0);
        System.out.println(s1);
        System.out.println(s2);
        //Rectangle
        Rectangle r1 = new Rectangle(3, 2);
        Rectangle r2 = new Rectangle(4, 3);
        System.out.println(r1);
        System.out.println(r2);

        Shape[] shape = {c1, c2, c3};
        for (int i = 0; i < shape.length; i++) {
            System.out.println(shape[i].getName() + " area :" + shape[i].calArea());
        }
        Shape[] shape1 = {s1, s2};
        for (int i = 0; i < shape1.length; i++) {
            System.out.println(shape1[i].getName() + " area :" + shape1[i].calArea());
        }
        Shape[] shape2 = {r1, r2};
        for (int i = 0; i < shape2.length; i++) {
            System.out.println(shape2[i].getName() + " area :" + shape2[i].calArea());
        }
    }

}
